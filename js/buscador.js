function search(){
    var themes = document.getElementById('themes');
    var song = document.forms["buscador"]["cancion"].value;
    event.preventDefault();
    historial(song);
    fetch("https://genius.p.rapidapi.com/search?q="+song, {
	"method": "GET",
	"headers": {
		"x-rapidapi-host": "genius.p.rapidapi.com",
		"x-rapidapi-key": "dcf846c736msh6ccd2d6975c05d6p1db062jsna80cae99e633"
	}
})
.then(response =>
	response.json()    //convierto lo que devuelve la api en JSON
)
.then(data => imprimir(data.response.hits)) //llamo a la funcion que va a imprimir los resultados
.catch(err => {
	console.log(err);
});

}

function imprimir(data){
    document.getElementById('themes').innerHTML = null;
    for (var i = 0; i < 5; i++) {
        var li = document.createElement("li");              //creo un elemento de tipo objeto de lista
        var a = document.createElement("a");                //creo un elemento de tipo link
        var form = document.createElement("form");
        var inp = document.createElement("input");
        inp.setAttribute("type","submit");
        inp.className = "form-input";
        inp.setAttribute("name","url");
        inp.value = "compartir";
        form.appendChild(inp);
        form.setAttribute("method","post");
        form.setAttribute("action","mail.html");
        form.setAttribute("onsubmit","return redir('"+data[i].result.url+"')");
        a.innerHTML = data[i].result.title+" de "+data[i].result.primary_artist.name;                 //pongo el nombre de la cancion en el link
        a.className = "link";                               //le asigno una clase al link
        a.href = data[i].result.url;                        //asigno el valor del href del link para que sea la url de la letra
        li.appendChild(a);
        li.appendChild(form);                              //agrego el link dentro del objeto de lista
        li.className = "resultado";                         //le asigno una clase al objeto de lista
        document.getElementById('themes').appendChild(li);  //asigno el objeto de lista a la lista

    }

}

function pop(){
    document.getElementById('link').value = sessionStorage.getItem('page');
}

function redir(url){
    sessionStorage.setItem("page", url);
    return true;
}

function fetchSong(song) {

    fetch("https://genius.p.rapidapi.com/songs/"+song, {
	"method": "GET",
	"headers": {
		"x-rapidapi-host": "genius.p.rapidapi.com",
		"x-rapidapi-key": "dcf846c736msh6ccd2d6975c05d6p1db062jsna80cae99e633"
	}
})
.then(response =>
    response.json()
)
.then(song =>
    console.log(song)
)
.catch(err => {
	console.log(err);
});

}

function historial(busqueda){
    var lh = document.createElement("li");
    lh.className = "historial-item";
    lh.innerHTML = busqueda;
    document.getElementById('log').appendChild(lh);
}
